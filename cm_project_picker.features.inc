<?php
/**
 * @file
 * cm_project_picker.features.inc
 */

/**
 * Implements hook_views_api().
 */
function cm_project_picker_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
